from django.shortcuts import render
from dashboard.models import Instance, Image
from django.db import IntegrityError
from django.http import *

from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django import template
from django.contrib.auth.models import User
import docker


def get_username(user_id):
    username = User.objects.all(user_id=user_id).username
    return username

def index(request):
    all_users = User.objects.all()
    all_instances = Instance.objects.all()
    context = {
        'all_instances': all_instances,
        'all_users': all_users,
    }
    currentuser = request.user
    if currentuser.is_superuser:
        return render(request, 'manager/index.html', context)
    else:
        return HttpResponseRedirect("/")

def login(request):
    return render(request, 'manager/login.html')
def domains(request):
    return HttpResponse("Under Construction")

def users(request):
    currentuser = request.user
    if currentuser.is_superuser:
        all_users = User.objects.all()
        context = {
            'all_users': all_users,
        }
        return render(request, 'manager/users.html', context)
    else:
        return HttpResponseRedirect("/")

def logout(request):
    auth_logout(request)
    return HttpResponseRedirect("/login/")


def run_container(request):
    currentuserid = request.user
    if currentuserid.is_superuser:
        containerid = request.GET['id']
        client = docker.from_env()
        newclient = client.containers.get(containerid)
        newclient.start()
        if client.containers.get(containerid).status == "running":
            p = Instance.objects.get(mwsubdomain=containerid)
            p.status = 1
            p.save()
        return HttpResponseRedirect("/manager/")
    else:
        return HttpResponse("Access Denied")

def stop_container(request):
    currentuserid = request.user
    if currentuserid.is_superuser:
        containerid = request.GET['id']
        user_instances = Instance.objects.all().filter(mwsubdomain=containerid)
        for instance in user_instances:
            client = docker.from_env()
            newclient = client.containers.get(containerid)
            newclient.stop()
            if client.containers.get(containerid).status == "exited":
                instance.status = 0
                instance.save()
            return HttpResponseRedirect("/manager/")
    else:
        return HttpResponse("Access Denied")

def deploy(request):
    if request.method == 'POST':
        instance_name = request.POST['appName']
        instance_domainname = request.POST['domainName']
        currentuserid = request.user.pk
        try:
            p = Instance(name=instance_name, domainname=instance_domainname, image_id=1, status=1, user_id=currentuserid)
            addsubdomain(p.mwsubdomain)
            p.save()
            client = docker.from_env()
            client.containers.run(Image(image_id=p.image_id).image_url, ports={'80/tcp': p.portno}, name=p.mwsubdomain, detach=True, environment={"VIRTUAL_HOST":"mw"+p.mwsubdomain+".cloud.migratewizard.com,"+p.domainname})
        except IntegrityError as e:
            if 'UNIQUE constraint' in e.args[0]:
                return HttpResponse(" Domain Already Exists")
        return HttpResponseRedirect("/manager/")
    return render(request, 'dashboard/deploy_new.html')

def del_container(request):
    currentuserid = request.user
    if currentuserid.is_superuser:
        containerid = request.GET['id']
        p = Instance.objects.filter(mwsubdomain=containerid)
        client = docker.from_env()
        for container in p:
            p.delete()
            newclient = client.containers.get(container_id=container.mwsubdomain)
            newclient.remove(force=True)
            return HttpResponseRedirect("/manager/")
    else:
        return HttpResponse("Access Denied")
#END Apps



#Start Users:

def del_user(request):
    currentuser = request.user
    username = request.GET['username']
    if currentuser.is_superuser:
        try:
            u = User.objects.get(username=username)
            u.delete()
            return HttpResponseRedirect("/manager/users/")
        except User.DoesNotExist:
            return HttpResponse("User Doesn't Exists")
