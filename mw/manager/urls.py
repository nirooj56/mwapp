from django.conf.urls import url
from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import RedirectView

app_name = 'manager'


urlpatterns = [
    url(r'^$', views.index, name="home"),
    url(r'^domains/$', views.domains, name="domains"),
    url(r'^users/$', views.users, name="users"),
    url(r'^logout/$', views.logout),

    # Apps
    url(r'^resume/$', views.run_container),
    url(r'^pause/$', views.stop_container),
    url(r'^deploy/$', views.deploy),
    url(r'^del/$', views.del_container),


    # Users
    url(r'^users/del/$', views.del_user),

]