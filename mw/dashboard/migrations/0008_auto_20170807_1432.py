# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-07 14:32
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0007_auto_20170801_0851'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Containers',
        ),
        migrations.RenameField(
            model_name='image',
            old_name='imagename',
            new_name='url',
        ),
        migrations.RenameField(
            model_name='instance',
            old_name='containerid',
            new_name='imageid',
        ),
    ]
