# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-25 13:24
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0015_auto_20170825_1323'),
    ]

    operations = [
        migrations.RenameField(
            model_name='instance',
            old_name='portno',
            new_name='port',
        ),
    ]
