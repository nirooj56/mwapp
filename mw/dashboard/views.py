from django.shortcuts import render, redirect
from .models import Instance, Image
from django.db import IntegrityError
import subprocess
# Create your views here.
from django.http import *
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.decorators import login_required
import docker

##Forms here
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from .forms import SignupForm
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from .tokens import account_activation_token
from django.contrib.auth.models import User

from django.core.mail import send_mail

##Celery
#from celery import shared_task

from .subdomain import addRecord


def index(request):
    currentuser = request.user
    userId = currentuser.pk
    all_instances = Instance.objects.all().filter(user_id=userId) #userId is the logged in active user.
    context = {
        'all_instances': all_instances,
    }
    return render(request, 'dashboard/welcome.html', context)

# Apps
def run_container(request):
    containerid = request.GET['id']
    currentuserid = request.user.pk
    user_instances = Instance.objects.all().filter(user_id=currentuserid, mwsubdomain=containerid)
    for instance in user_instances:
        client = docker.from_env()
        newclient = client.containers.get(containerid)
        newclient.start()
        if client.containers.get(containerid).status == "running":
            p = Instance.objects.get(mwsubdomain=containerid)
            p.status = 1
            p.save()
        return HttpResponseRedirect("/")

def stop_container(request):
    containerid = request.GET['id']
    currentuserid = request.user.pk
    user_instances = Instance.objects.all().filter(user_id=currentuserid, mwsubdomain=containerid)
    for instance in user_instances:
        client = docker.from_env()
        newclient = client.containers.get(containerid)
        newclient.stop()
        if client.containers.get(containerid).status == "exited":
            instance.status = 0
            instance.save()
        return HttpResponseRedirect("/")

def deploy(request):
    if request.method == 'POST':
        instance_name = request.POST['appName']
        instance_domainname = request.POST['domainName']
        currentuser = request.user
        user_instances = Instance.objects.all().filter(user_id=currentuser.pk)
        if currentuser.profile.limit > user_instances.count():
            try:
                p = Instance(name=instance_name, domainname=instance_domainname, image_id=1, status=1, user_id=currentuser.pk)
                addsubdomain(p.mwsubdomain)
                p.save()
                client = docker.from_env()
                client.containers.run(Image(image_id=p.image_id).image_url, ports={'80/tcp': p.portno}, name=p.mwsubdomain, detach=True, environment={"VIRTUAL_HOST":"mw"+p.mwsubdomain+".cloud.migratewizard.com,"+p.domainname})
            except IntegrityError as e:
                if 'UNIQUE constraint' in e.args[0]:
                    return HttpResponse(" Domain Already Exists")
            return HttpResponseRedirect("/")
        else:
            return HttpResponse("Your instance limit has crossed.")
    return render(request, 'dashboard/deploy_new.html')

def del_container(request) :
    containerid = request.GET['id']
    currentuserid = request.user.pk
    p = Instance.objects.filter(user_id=currentuserid, mwsubdomain=containerid)
    client = docker.from_env()
    for container in p:
        p.delete()
        newclient = client.containers.get(container_id=container.mwsubdomain)
        newclient.remove(force=True)
        return HttpResponseRedirect("/")
#END Apps


#Domains
def domains(request):
    return HttpResponseRedirect('/')

def addsubdomain(subdomain):
    addRecord(subdomain)

#Billing
def billing(request):
    return HttpResponseRedirect("/")


def profile(request):
    return HttpResponseRedirect("/")



#Login and Signups !

def login(request):
    return render(request, 'dashboard/login.html')


def auth(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)
    if user is not None:
        auth_login(request, user)
        if user.is_superuser:
            return HttpResponseRedirect('/manager/')
        return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/login/invalid')


def invalid(request):
    return HttpResponse('Invalid Login :) Try Again')


def signup(request):
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()
            current_site = get_current_site(request)
            subject = 'Migrate Wizard Account Activation Code'
            message = render_to_string('account_activation_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': account_activation_token.make_token(user),
            })

           #user.email_user(subject, message)
            to_email = form.cleaned_data.get('email')
            send_mail(subject, message,'noreply@cloud.migratewizard.com', [to_email], fail_silently=False)
            return HttpResponse('Please confirm your email address to complete the registration')
    else:
        form = SignupForm()
    return render(request, 'dashboard/signup.html', {'form': form})


def logout(request):
    auth_logout(request)
    return HttpResponseRedirect("/login/")


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        auth_login(request, user)
        return HttpResponseRedirect("/")
    else:
        return HttpResponse('Activation link is invalid!')