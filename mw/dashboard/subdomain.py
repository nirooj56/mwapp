import os
#Add DNS records for the added app.

def reloaddns():
    os.system("sudo /usr/sbin/service bind9 reload")

def addRecord(subdomain):
    zone_file_path ='/etc/bind/zones/cloud.migratewizard.com.db'
    record = "\n"+"mw"+subdomain+" IN CNAME @"
    with open(zone_file_path, 'a') as f:
        f.write(record)
    f.close()
    reloaddns()
