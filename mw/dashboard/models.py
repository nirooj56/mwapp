from __future__ import unicode_literals

from django.db import models
from random import randrange
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

def get_port(port=None):
    generate = lambda: format(randrange(8000, 9000, 1))
    if not port:
        port = generate()
    while Instance.objects.filter(portno=port):
        port = generate()
    return port
def get_subdomain(subdomain=None):
    generate = lambda: format(randrange(100, 400, 1))
    if not subdomain:
        subdomain = generate()
    while Instance.objects.filter(mwsubdomain=subdomain):
        subdomain = generate()
    return subdomain

###### Docker Apps ############
class Instance(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=30)
    user = models.ForeignKey(User)  # instead of user_id = IntegerField()
    domainname = models.CharField(max_length=40, blank=True, unique=True)
    mwsubdomain = models.CharField(max_length=30, default=get_subdomain)
    image_id = models.CharField(max_length=10)
    portno = models.CharField(max_length=9000, unique=True, default=get_port)
    status = models.IntegerField(default=0)

    def __str__(self):
        return self.name + '-' + self.mwsubdomain


class Image(models.Model):
    image_id = models.IntegerField(primary_key=True)
    image_name = models.CharField(max_length=30, default="WordPress") #Image Name from the repo
    image_url = models.CharField(max_length=40, default="tutum/wordpress") #Image Name for the site.

############# DOMAIN ################################

class domains(models.Model):
    domain_id = models.IntegerField(unique=True, primary_key=True)
    domain_name = models.CharField(max_length=500)
    registered_date = models.DateField()
    expiry_date = models.DateField()

    def __str__(self):
        return self.domain_name


#### User Profile ###################
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    limit = models.IntegerField(default=3)
    email_confirmed = models.BooleanField(default=False)

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()