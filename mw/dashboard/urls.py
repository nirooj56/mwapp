from django.conf.urls import url
from . import views

from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.decorators import login_required
from decorator_include import decorator_include
app_name = 'dashboard'


urlpatterns = [
    url(r'^$', views.index, name="home"),

    #Apps
    url(r'^resume/$', views.run_container),
    url(r'^pause/$', views.stop_container),
    url(r'^deploy/$', views.deploy),
    url(r'^del/$', views.del_container),

    #Domains
    url(r'^domains/$', views.domains),



    #Billing
    url(r'^billing/$', views.billing),

    #Profile
    url(r'^profile/$', views.profile),

    #Session Handling
    url(r'^login/$', views.login, name='login'),
    url(r'^login/auth/$', views.auth),
    url(r'^login/invalid/$', views.invalid),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^logout/$', views.logout),

]
